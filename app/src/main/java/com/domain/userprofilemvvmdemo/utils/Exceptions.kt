package com.domain.userprofilemvvmdemo.utils

import java.lang.Exception

class DataBindingException : Exception()