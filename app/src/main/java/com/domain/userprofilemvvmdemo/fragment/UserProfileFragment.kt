package com.domain.userprofilemvvmdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.domain.userprofilemvvmdemo.R
import com.domain.userprofilemvvmdemo.databinding.FragmentUserProfileBinding
import com.domain.userprofilemvvmdemo.viewmodel.UserViewModel

class UserProfileFragment : Fragment() {
    lateinit var fragmentUSerProfileBinding: FragmentUserProfileBinding
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentUSerProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user_profile, container, false)
        fragmentUSerProfileBinding.lifecycleOwner = viewLifecycleOwner
        fragmentUSerProfileBinding.userProfileVIewModel = userViewModel

        return fragmentUSerProfileBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentUSerProfileBinding.submit.setOnClickListener {
            findNavController(this).navigate(R.id.action_userprofilefragment_to_userdetailfragment)
        }
    }
}
