package com.domain.userprofilemvvmdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.domain.userprofilemvvmdemo.R
import com.domain.userprofilemvvmdemo.databinding.FragmentUserDetailBinding
import com.domain.userprofilemvvmdemo.viewmodel.UserViewModel

class UserDetailFragment : Fragment() {
    lateinit var fragmentUserDetailBinding: FragmentUserDetailBinding
    private val userDetailViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentUserDetailBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user_detail, container, false)
        fragmentUserDetailBinding.lifecycleOwner = viewLifecycleOwner
        fragmentUserDetailBinding.userDetailViewModel = userDetailViewModel
        return fragmentUserDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val selectedDate =
            "" + resources.getStringArray(R.array.day)[userDetailViewModel.spinnerSelectedDayPosition.value!!] +
                    "-" + resources.getStringArray(R.array.month)[userDetailViewModel.spinnerSelectedMonthPosition.value!!] +
                    "-" + resources.getStringArray(R.array.year)[userDetailViewModel.spinnerSelectedYearPosition.value!!]

        userDetailViewModel.getAge(selectedDate)
        fragmentUserDetailBinding.backButton.setOnClickListener {
            findNavController().navigate(R.id.action_userdetailfragment_to_userprofilefragment)
        }
    }
}