package com.domain.userprofilemvvmdemo.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class UserViewModel : ViewModel() {
    val firstName = ObservableField<String>()
    val lsstName = ObservableField<String>()
    val age = ObservableField<String>()
    val spinnerSelectedDayPosition = MutableLiveData<Int>(0)
    val spinnerSelectedMonthPosition = MutableLiveData<Int>(0)
    val spinnerSelectedYearPosition = MutableLiveData<Int>(0)

    fun getAge(date: String) {
        val currentDate = Date()
        val format = SimpleDateFormat("dd-MM-yyyy")
        val selectedDate = format.parse(date)
        val time = currentDate.time - selectedDate.time
        var dy: Long = TimeUnit.MILLISECONDS.toDays(time)
        val yr = dy / 365
        dy %= 365
        val mn = dy / 30
        dy %= 30;
        val wk = dy / 7
        dy %= 7
        val days = (wk * 7) + dy - 1
        age.set("$yr years $mn months $days days")
    }

}